import uuid
from autosky.logger import AutoSkyLogger
from apps.painel.models import Parceiro

log = AutoSkyLogger('usuario')

log.info('script_remove_usuario: start script')

par_uuid = '79522140-ce69-4d57-ae7e-d8941e9fb4ae'

extra = {'par_uuid': par_uuid}

process_uuid = uuid.uuid4()

parceiro = Parceiro.objects.get(par_uuid=par_uuid)

clientes = parceiro.cli.all()


for cliente in clientes:

    log.debug(
        "script_remove_usuario: "
        "got user list to remove from client. "
        "client: {} ".format(
            cliente.cli_uuid
        ),
        extra=extra
    )

    usuarios = cliente.usuario_set.all()
    # Is there a valid user
    if not usuarios:
        # Error and we don't do anything, just reload the page
        log.warning("script_remove_usuario: not user was found.",
                    extra=extra
                    )

    # Remove User from Client
    for usuario in usuarios:
        usr_uuid = usuario.usr_uuid
        extra['usuario'] = usr_uuid
        if usuario.ad_del_user_from_client(cliente):
            log.info(
                "script_remove_usuario: "
                "user removed from client. "
                "user_uuid: {} "
                "cli_uuid: {} ".format(
                    usr_uuid,
                    cliente.cli_uuid,
                ),
                extra=extra
            )

            # Was this the last Client assigned to this user?
            if usuario.clients.count() == 0:
                # If so, remove the user from AD
                log.info(
                    "script_remove_usuario: "
                    "user is not present in any other client, "
                    "removing it from active directory (AD). "
                    "user_uuid: {} ".format(
                        usr_uuid
                    ),
                    extra=extra
                )

                if usuario.ad_remove_usuario(cliente.parceiro):
                    # And also from our DB
                    usuario.delete()
                    log.info(
                        "script_remove_usuario: "
                        "user removed from active directory (AD). "
                        "user_uuid: {} ".format(
                            usr_uuid,
                        ),
                        extra=extra
                    )
                # We failed to remove user from AD
                else:

                    log.warning(
                        "script_remove_usuario: "
                        "fail to delete user from active directory(AD). "
                        "user_uuid: {} ".format(
                            usr_uuid,
                        ),
                        extra=extra
                    )
            # Check if we need to delete the userads related to
            # client.parceiro
            else:
                usuario.update_user_ads(cliente.parceiro, cliente, extra)
        # We failed to remove user from client
        else:
            log.error(
                "script_remove_usuario: "
                "fail to remove user from client. "
                "user_uuid: {} "
                "cli_uuid: {} ".format(
                    usr_uuid,
                    cliente.cli_uuid,
                ),
                extra=extra
            )
log.info('script_remove_usuario: end script')

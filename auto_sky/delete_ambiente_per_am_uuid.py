from apps.painel.models import Ambiente
from autosky.logger import AutoSkyLogger
from apps.painel.commons import delete_abo_container
from django.db import IntegrityError
from apps.agenda.models import Action
from apps.clientes.models import Cliente

log = AutoSkyLogger('painel')

amb_uuids = [
    "c075f69d-bf16-4b5a-b917-2a100f09bf8f", "71c30ac6-9b1f-46e9-a46a-0e787b2188e7", "42e689bd-1bda-47ef-8962-f3517e0b07dd", "f86e5fbb-a2c3-47d0-bdfa-53af926ecb43", "bab972c5-6410-4e22-b678-7506a413bcd3", "b14f9d60-4555-40f0-8d01-2497fe767384", "46e35e74-8737-49e6-b328-bf5b058f0db8", "903b4e98-b611-4223-bd7b-0fe1981731b5", "f86694b1-71bf-49a8-ab6c-e8ea7ef354dd", "83b6edc2-7253-4490-a418-cb0d09c3dbc4", "cc7bbf71-2582-4051-be37-1b3ebbaa00b4", "e8c2d5bb-bac1-4b01-8b56-5a2ab885d7ca", "6aaf91b8-45ad-4961-9589-5602888d0c09", "54f9ff04-3efb-46f9-8d39-2b2c87876385", "3da98d2e-21ea-400c-b65d-74c815b5351a", "18b25831-e654-4fda-a43e-a061310d381e", "5356922d-9c98-4a75-ae60-31f76b83ab9b", "5842623c-d3e0-4d4b-bb96-194ec0441609", "50a0a56a-8db4-486c-94f3-e45534eda30f", "1881c649-2dd0-4833-bd18-acfdf2dc9059", "550f28e3-9f6a-4fc5-ac76-b757a3d69a1d", "0cad3c64-1718-4a10-a83f-7684f1a6a8b4", "2b7bd574-dfb6-491b-ae5c-aa92d3cdd99b", "ad4c3cdc-1336-41b4-9dc3-17e6d37b67bd", "966650fb-51bb-495a-9b2a-712ed75f0907", "5f90a733-7457-4098-93a6-8a29dfc11cfb", "bd836249-e3dc-420e-a37c-076146df40c9", "341d2940-3a06-4e89-82e8-a2bf38c7c7a4", "ae41e3fa-2013-47f0-a0bf-686ae0abe2ac", "d9539c63-5819-4057-8526-9acefdc57cc4", "c0973819-880a-4cab-90bd-b13cffbb3cf7", "3d19330a-9f2a-4c92-b83e-c3eddf43bb64", "8e2394f0-b82b-4801-9d35-6f0b38580e8c", "7d54933f-6fe7-48d9-ba73-8064c88afbaa", "36d81239-2ee2-4a43-925c-9e1f760d9673", "d6d4ff16-2299-4246-a338-e4a4ca84045a", "e15c926e-2b89-4936-8613-4b06409795c9", "526a0bb7-f2c6-4814-80d2-0a5a712e4ffe", "92c48ddb-afc4-439f-ae83-94a5a88ef5de"]

for amb_uuid in amb_uuids:

    try:
        ambiente = Ambiente.objects.get(amb_uuid=amb_uuid)
    except:
        print(f'Ambiente not found {amb_uuid}')
        continue

    partner = ambiente.parceiro
    extra = {
        'ambiente': ambiente.amb_uuid,
        'parceiro': partner.par_uuid,
    }
    clientes = Cliente.objects.filter(amb_uuid=ambiente.amb_uuid).count()
    instancias = ambiente.ins.count()
    if clientes or instancias:
        html_data = {
            'ambiente': ambiente,
            'partner': partner,
            'clientes': clientes,
            'instancias': instancias
        }
        print(f'Ambiente {ambiente.nome}  do parceiro {partner.nome} com instancia {instancias} clientes {clientes } ')
        continue

    try:
        # If the product is ABO and it has an abo_key, it means this
        # environment has a container created and we should delete it first
        # if ambiente.product.product == 'ABO' and ambiente.abo_key:
        #     response = delete_abo_container(ambiente, 'request_uuid')
        #     if 'errors' in response:
        #         print(
        #             "remove_ambiente: error deleting abo container from "
        #             "environment. container:%s environment_uuid:%s nome: %s" % (ambiente.abo_key, ambiente.amb_uuid, ambiente.nome))
        #         continue


        amb_nome = ambiente.get_name()
        amb_uuid = ambiente.amb_uuid
        print(f'vai deletar {amb_uuid}')
        ambiente.delete()
    except IntegrityError as e:
        log.exception('remove_ambiente: error trying to remove environment. '
                        'environment:{}'.format(ambiente.amb_uuid))
    else:
        print('remove_ambiente: environment removed. '
                    'environment:%s environment_uuid: %s'
                    % (amb_nome, amb_uuid))

        start_action = Action.objects.filter(
            action_type='block_instance_with_old_publication',
            params__contains=amb_uuid)
        start_action.delete()

        print('remove_ambiente: deleted block_instance_with_old_publication'
                    ' for environment. environment:%s nome %s' % (amb_uuid, ambiente.nome))

import pandas as pd
from sqlalchemy import create_engine
# pg_engine = create_engine('postgresql://ipaas:Y0X0UqrBNObR@3.223.146.115:5432/ipaas')
pg_engine = create_engine('postgresql://ipaas:Y0X0UqrBNObR@localhost:5432/ipaas')
# mysql_engine = create_engine('mysql+mysqldb://ipaas:CQxmeb8MwmLc@3.223.146.115/ipaas')
# mssql_engine = create_engine('mssql+pymssql://sa:Y0X0UqrBNObR@3.223.146.115/ipaas')
pg_con =  pg_engine.connect()
# pg_con =  mysql_engine.connect()
# pg_con =  mssql_engine.connect()
# pg_engine = create_engine('oracle+cx_oracle://system:password@localhost:1521/XE', encoding="UTF-8",connect_args={ 
    # ...:         "mode": cx_Oracle.SYSDBA, 
    # ...:         "events": True 
    # ...:     })  

df_client = pd.read_csv('clientes_export.csv')
df_cnpj_small = pd.read_csv('cnpj_small_export.csv', sep=',')
df_cnpj_small['cnpj'] = df_cnpj_small['cnpj'].astype(str)
df_cnpj_large = pd.read_csv('cnpj_large_export.csv', sep=',')
df_cnpj_large['cnpj'] = df_cnpj_large['cnpj'].astype(str)


print('import client')
df_client.to_sql('client', pg_con, if_exists='replace', index=False)
print('import small')
df_cnpj_small.to_sql('small_cnpj', pg_con, if_exists='replace', index=False)
print('import large')
df_cnpj_large.to_sql('large_cnpj', pg_con, if_exists='replace', index=False, chunksize=1000, method='multi')


from sqlalchemy import create_engine
import pandas as pd
import unidecode
from pandas.api.types import is_string_dtype 

# tns = """
#   (DESCRIPTION =
#     (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))
#     (CONNECT_DATA =
#       (SERVER = DEDICATED)
#       (SERVICE_NAME = XE)
#     )
#   )
# """

# usr = "system"
# pwd = "password"
# engine = create_engine('oracle+cx_oracle://%s:%s@%s' % (usr, pwd, tns))
# con = engine.connect()

def import_oracle():
    
    tns = """
    (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = 3.223.146.115)(PORT = 1521))
    (CONNECT_DATA =
        (SERVER = DEDICATED)
        (SERVICE_NAME = XE)
    )
    )
    """

    usr = "system"
    pwd = "pwYRAoF8nJxE"
    engine = create_engine('oracle+cx_oracle://%s:%s@%s' % (usr, pwd, tns))
    con = engine.connect()

    # df_client = pd.read_csv('clientes_export.csv', encoding = 'utf8')
    # df_client = remove_special_characters(df_client)

    # df_cnpj_small = pd.read_csv('cnpj_small_export.csv', sep=',',encoding = 'utf8')
    # df_cnpj_small['cnpj'] = df_cnpj_small['cnpj'].astype(str)
    # df_cnpj_small = remove_special_characters(df_cnpj_small)

    df_cnpj_large = pd.read_csv('cnpj_large_export.csv', sep=',', encoding = 'utf8')
    df_cnpj_large['cnpj'] = df_cnpj_large['cnpj'].astype(str)
    df_cnpj_large = remove_special_characters(df_cnpj_large)


    # print('import client')
    # df_client.to_sql('client', con, if_exists='replace', index=False, schema='IPAAS')
    # print('import small')
    # df_cnpj_small.to_sql('small_cnpj', con, if_exists='replace', index=False, schema='IPAAS')
    print('import large')
    df_cnpj_large.to_sql('large_cnpj', con, if_exists='replace', index=False, schema='IPAAS')

def remove_special_characters(dataframe):
    # print(dataframe)
    for col in dataframe.columns:
        if is_string_dtype(dataframe[col]):
            # print(col)
            dataframe[col] = dataframe[col].astype(str)
            dataframe[col].replace(regex=True, inplace=True, to_replace=r'[^a-zA-Z\d\s]', value=r'')
            dataframe[col].apply(unidecode.unidecode)
    # print(dataframe)

    return dataframe

import_oracle()